# Note
```
guid: qqswDRb--P
notetype: Basic
```

### Tags
```
```

## Front
What does <strong>kube-proxy</strong> do?

## Back
The <strong>kube-proxy</strong> creates and manages networking
rules to expose the container on the network to other containers or
the outside world.
