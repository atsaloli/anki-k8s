# Note
```
guid: w7?,`@Q^p.
notetype: Basic
```

### Tags
```
```

## Front
How can you communicate with the API server?

## Back
You can communicate with the API using a local client called
<strong>kubectl</strong>, Or You can write your own client and use
<strong>curl</strong> commands.
